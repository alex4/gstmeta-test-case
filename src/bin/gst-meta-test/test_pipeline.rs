use gst::prelude::*;
use gst_meta::custom_meta;


fn make_source_elems(source_path: &String, i: u32) -> (gst::Element, gst::Element) {
    let source_name = format!("source{}", i);
    let source = gst::ElementFactory::make("filesrc", Some(&source_name))
        .expect(&format!("Could not create {} element.", source_name));

    let h264parser_name = format!("h264parser{}", i);
    let h264parser = gst::ElementFactory::make("h264parse", Some(&h264parser_name))
        .expect(&format!("Could not create {} element.", h264parser_name));

    source.set_property_from_str("location", source_path);
    return (source, h264parser);
}

fn run_pipeline(source_path: &String, out_path: &String) {
    gst::init().unwrap();

    let (source0, h264parser0) = make_source_elems(source_path, 0);
    let (source1, h264parser1) = make_source_elems(source_path, 1);

    let concat = gst::ElementFactory::make("concat", Some("concat"))
        .expect("Could not create concat element.");

    let decoder = gst::ElementFactory::make("nvv4l2decoder", Some("nvv4l2-decoder"))
        .expect("Could not create decoder element.");

    let sample_plugin = gst::ElementFactory::make("sample_plugin", Some("sample_plugin"))
        .expect("Could not create sample_plugin element");

    let encoder = gst::ElementFactory::make("nvv4l2h264enc", Some("nvv4l2-encoder"))
        .expect("Could not create encoder element");

    let qtmux =
        gst::ElementFactory::make("qtmux", Some("qt-mux")).expect("Could not create qtmux element");

    let h264parse_enc = gst::ElementFactory::make("h264parse", Some("h264-parser-enc"))
        .expect("Could not create h264parse_enc element");
    let sink =
        gst::ElementFactory::make("filesink", Some("sink")).expect("Could not create sink element");

    sink.set_property_from_str("location", out_path);

    // Create the empty pipeline
    let pipeline = gst::Pipeline::new(Some("select"));

    // Build the pipeline
    pipeline
        .add_many(&[
            &concat,
            &decoder,
            &sample_plugin,
            &encoder,
            &h264parse_enc,
            &qtmux,
            &sink,
        ])
        .unwrap();

    pipeline.add_many(&[&source0, &h264parser0]).unwrap();

    pipeline.add_many(&[&source1, &h264parser1]).unwrap();

    source0
        .link(&h264parser0)
        .expect("Couldn't link source0 to h264parser");
    h264parser0
        .link_pads(Some("src"), &concat, Some("sink_0"))
        .expect("Couldn't link h264parser0 to concat");

    source1
        .link(&h264parser1)
        .expect("Couldn't link source1 to h264parser");
    h264parser1
        .link_pads(Some("src"), &concat, Some("sink_1"))
        .expect("Couldn't link h264parser1 to concat");
    // This video stream doesn't have a notion of PTS, so just set the PTS to the decoding time, otherwise
    // we get an error that Buffer has no PTS.")
    h264parse_enc.get_static_pad("src").unwrap().add_probe(
        gst::PadProbeType::BUFFER,
        move |_pad, info| {
            if let Some(gst::PadProbeData::Buffer(buffer)) = &mut info.data {
                let dts = buffer.get_dts();
                buffer.make_mut().set_pts(dts);
            }
            gst::PadProbeReturn::Pass
        },
    );

    concat
        .link(&decoder)
        .expect("Couldn't link concat to decoder");

    decoder
        .link(&sample_plugin)
        .expect("Elements could not be linked 4.");

    decoder.get_static_pad("src").unwrap().add_probe(
        gst::PadProbeType::BUFFER,
        |_pad, info| {
            if let Some(gst::PadProbeData::Buffer(ref mut buffer)) = &mut info.data {
                {
                    let buffer = buffer.make_mut();
                    let label = "test label from pipeline";
                    custom_meta::CustomMeta::add(buffer, label.to_string());
                }
            }
            gst::PadProbeReturn::Ok
        },
    );

    sample_plugin
        .link(&encoder)
        .expect("Elements could not be linked 5.");

    encoder
    
        .link(&h264parse_enc)
        .expect("Elements could not be linked 6.");

    h264parse_enc
        .link(&qtmux)
        .expect("Elements could not be linked 7.");
    qtmux.link(&sink).expect("Elements could not be linked 7.");

    // Start playing
    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    // Wait until error or EOS
    let bus = pipeline.get_bus().unwrap();
    for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
        use gst::MessageView;

        match msg.view() {
            MessageView::Error(err) => {
                eprintln!(
                    "Error received from element {:?}: {}",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error()
                );
                eprintln!("Debugging information: {:?}", err.get_debug());
                break;
            }
            MessageView::Eos(..) => break,
            _ => (),
        }
    }

    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");
}

fn main() {
    let matches = clap::App::new("OpticalFlow")
        .about("Gst pipeline demonstrating deepstream optical flow")
        .arg(
            clap::Arg::with_name("source")
                .long("source")
                .takes_value(true),
        )
        .arg(clap::Arg::with_name("out").long("out").takes_value(true))
        .get_matches();
    let source_path = String::from(
        matches
            .value_of("source")
            .unwrap_or("/opt/nvidia/deepstream/deepstream-5.0/samples/streams/sample_720p.h264"),
    );
    let out_path = String::from(
        matches
            .value_of("out")
            .unwrap_or("/opt/nvidia/deepstream/deepstream-5.0/out.mp4"),
    );
    run_pipeline(&source_path, &out_path);
}
