## Instructions

```
docker build -t video-accel . -f docker/video-accel/Dockerfile
docker run --gpus all -v $PWD:/src/inokyo -v $PWD/data:/data --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -it --rm video-accel bash

cd lib/gst-meta
cargo build --release

cargo run --example custom_meta
cargo run --example custom_meta_base_example

cd ../gst-plugins
cargo build --release

export GST_PLUGIN_PATH=/src/inokyo/lib/gst-plugins/target/release/

cd ../..
cargo build --release

./target/release/test_pipeline
```
