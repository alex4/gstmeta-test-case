mod sample_plugin;

// Plugin entry point that should register all elements provided by this plugin,
// and everything else that this plugin might provide (e.g. typefinders or device providers).
fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    sample_plugin::register(plugin)?;
    Ok(())
}

// Static plugin metdata that is directly stored in the plugin shared object and read by GStreamer
// upon loading.
// Plugin name, plugin description, plugin entry point function, version number of this plugin,
// license of the plugin, source package name, binary package name, origin where it comes from
// and the date/time of release.

gst::plugin_define!(
    sample_plugin,
    "sample plugins",
    plugin_init,
    "v1.0.0",
    "MIT/X11",
    "sample_plugin",
    "sample_plugin",
    "https://gitlab.freedesktop.org/alex4/gstmeta-test-case",
    "2021-01-20"
);
