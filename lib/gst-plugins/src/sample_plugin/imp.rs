use glib::subclass;
use glib::subclass::prelude::*;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst::{gst_log, gst_trace};
use once_cell::sync::Lazy;
use gst_meta::custom_meta;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "sample_plugin",
        gst::DebugColorFlags::empty(),
        Some("SamplePlugin Element"),
    )
});

pub struct SamplePlugin {
    srcpad: gst::Pad,
    sinkpad: gst::Pad,
}

impl SamplePlugin {

    fn sink_chain(
        &self,
        pad: &gst::Pad,
        _element: &super::SamplePlugin,
        buffer: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        gst_log!(CAT, obj: pad, "Handling buffer {:?}", buffer);

        let meta = buffer
            .get_meta::<custom_meta::CustomMeta>()
            .expect("No custom meta found");
        match meta.get_label().is_empty() {
            false => {
                self.srcpad.push(buffer)
            },
            true =>  {
                Ok(gst_base::BASE_TRANSFORM_FLOW_DROPPED)
            }
        }
    }

    fn sink_event(&self, pad: &gst::Pad, _element: &super::SamplePlugin, event: gst::Event) -> bool {
        gst_log!(CAT, obj: pad, "Handling event {:?}", event);
        self.srcpad.push_event(event)
    }

    fn sink_query(
        &self,
        pad: &gst::Pad,
        _element: &super::SamplePlugin,
        query: &mut gst::QueryRef,
    ) -> bool {
        gst_log!(CAT, obj: pad, "Handling query {:?}", query);
        self.srcpad.peer_query(query)
    }

    fn src_event(&self, pad: &gst::Pad, _element: &super::SamplePlugin, event: gst::Event) -> bool {
        gst_log!(CAT, obj: pad, "Handling event {:?}", event);
        self.sinkpad.push_event(event)
    }

    fn src_query(
        &self,
        pad: &gst::Pad,
        _element: &super::SamplePlugin,
        query: &mut gst::QueryRef,
    ) -> bool {
        gst_log!(CAT, obj: pad, "Handling query {:?}", query);
        self.sinkpad.peer_query(query)
    }
}


impl ObjectSubclass for SamplePlugin {
    const NAME: &'static str = "sample_plugin";
    type Type = super::SamplePlugin;
    type ParentType = gst::Element;
    type Instance = gst::subclass::ElementInstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib::object_subclass!();

    fn with_class(klass: &Self::Class) -> Self {
        let templ = klass.get_pad_template("sink").unwrap();
        let sinkpad = gst::Pad::builder_with_template(&templ, Some("sink"))
            .chain_function(|pad, parent, buffer| {
                SamplePlugin::catch_panic_pad_function(
                    parent,
                    || Err(gst::FlowError::Error),
                    |sample_plugin, element| sample_plugin.sink_chain(pad, element, buffer),
                )
            })
            .event_function(|pad, parent, event| {
                SamplePlugin::catch_panic_pad_function(
                    parent,
                    || false,
                    |sample_plugin, element| sample_plugin.sink_event(pad, element, event),
                )
            })
            .query_function(|pad, parent, query| {
                SamplePlugin::catch_panic_pad_function(
                    parent,
                    || false,
                    |sample_plugin, element| sample_plugin.sink_query(pad, element, query),
                )
            })
            .build();

        let templ = klass.get_pad_template("src").unwrap();
        let srcpad = gst::Pad::builder_with_template(&templ, Some("src"))
            .event_function(|pad, parent, event| {
                SamplePlugin::catch_panic_pad_function(
                    parent,
                    || false,
                    |sample_plugin, element| sample_plugin.src_event(pad, element, event),
                )
            })
            .query_function(|pad, parent, query| {
                SamplePlugin::catch_panic_pad_function(
                    parent,
                    || false,
                    |sample_plugin, element| sample_plugin.src_query(pad, element, query),
                )
            })
            .build();

        Self {  srcpad, sinkpad }
    }

    fn class_init(klass: &mut Self::Class) {
        klass.set_metadata(
            "SamplePlugin",
            "Generic",
            "sample plugin",
            "Ryan Brigden <ryan@inokyo.com>, Alex Jiang <alex@inokyo.com>",
        );

        let caps = gst::Caps::new_any();
        let src_pad_template = gst::PadTemplate::new(
            "src",
            gst::PadDirection::Src,
            gst::PadPresence::Always,
            &caps,
        )
        .unwrap();
        klass.add_pad_template(src_pad_template);

        let sink_pad_template = gst::PadTemplate::new(
            "sink",
            gst::PadDirection::Sink,
            gst::PadPresence::Always,
            &caps,
        )
        .unwrap();
        klass.add_pad_template(sink_pad_template);
    }
}

impl ObjectImpl for SamplePlugin {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.add_pad(&self.sinkpad).unwrap();
        obj.add_pad(&self.srcpad).unwrap();
    }
}

impl ElementImpl for SamplePlugin {
    fn change_state(
        &self,
        element: &Self::Type,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        gst_trace!(CAT, obj: element, "Changing state {:?}", transition);
        self.parent_change_state(element, transition)
    }
}

