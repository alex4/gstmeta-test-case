use glib::prelude::*;
mod imp;

// The public Rust wrapper type for our element
glib::wrapper! {
    pub struct SamplePlugin(ObjectSubclass<imp::SamplePlugin>) @extends gst_base::BaseTransform, gst::Element, gst::Object;
}

// GStreamer elements need to be thread-safe. For the private implementation this is automatically
// enforced but for the public wrapper type we need to specify this manually.
unsafe impl Send for SamplePlugin {}
unsafe impl Sync for SamplePlugin {}

// Registers the type for our element, and then registers in GStreamer under
// the name "sample_plugin" for being able to instantiate it via e.g.
// gst::ElementFactory::make().
pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "sample_plugin",
        gst::Rank::None,
        SamplePlugin::static_type(),
    )
}
