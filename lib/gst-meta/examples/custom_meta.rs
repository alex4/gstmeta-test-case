use gst_meta::custom_meta;
use gst::element_error;
use gst::prelude::*;
use gst::meta::ReferenceTimestampMeta;

fn example_main() {
    gst::init().unwrap();

    // This creates a pipeline with appsrc and appsink.
    let pipeline = gst::Pipeline::new(None);
    let appsrc = gst::ElementFactory::make("appsrc", None)
        .unwrap()
        .downcast::<gst_app::AppSrc>()
        .unwrap();
    let appsink = gst::ElementFactory::make("appsink", None)
        .unwrap()
        .downcast::<gst_app::AppSink>()
        .unwrap();

    pipeline.add(&appsrc).unwrap();
    pipeline.add(&appsink).unwrap();
    appsrc.link(&appsink).unwrap();


    // Our buffer counter, that is stored in the mutable environment
    // of the closure of the need-data callback.
    let mut i = 0;
    appsrc.set_callbacks(
        gst_app::AppSrcCallbacks::builder()
            .need_data(move |appsrc, _| {
                // We only produce 5 buffers.
                if i == 5 {
                    let _ = appsrc.end_of_stream();
                    return;
                }

                println!("Producing buffer {}", i);

                // Add a custom meta with a label to this buffer.
                let mut buffer = gst::Buffer::new();
                {
                    let _ = buffer.get_mut().unwrap();
                    let label = match i % 2 == 0 {
                        true => "test label from example",
                        false => "",
                    };
                    // custom_meta::CustomMeta::add(buffer, label);
                    // ReferenceTimestampMeta::add(
                    //     buffer,
                    //     &caps::from_string("timestamp/x-raw"),
                    //     gst::ClockTime::from_nseconds(1234567891234567890),
                    //     gst::CLOCK_TIME_NONE,
                    // );
                }

                i += 1;

                // appsrc already handles the error here for us.
                let _ = appsrc.push_buffer(buffer);
            })
            .build(),
    );

    // Getting data out of the appsink is done by setting callbacks on it.
    // The appsink will then call those handlers, as soon as data is available.
    appsink.set_callbacks(
        gst_app::AppSinkCallbacks::builder()
            // Add a handler to the "new-sample" signal.
            .new_sample(|appsink| {
                // Pull the sample in question out of the appsink's buffer.
                let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;
                let buffer = sample.get_buffer().ok_or_else(|| {
                    element_error!(
                        appsink,
                        gst::ResourceError::Failed,
                        ("Failed to get buffer from appsink")
                    );

                    gst::FlowError::Error
                })?;

                // Retrieve the custom meta from the buffer and print it.
                let meta = buffer
                    .get_meta::<custom_meta::CustomMeta>()
                    .expect("No custom meta found");
                // let reference_timestamp_meta = buffer
                //     .get_meta::<ReferenceTimestampMeta>()
                //     .expect("No reference timestamp meta found");

                println!(
                    "Got buffer with label: {}",
                    meta.get_label(),
                    // reference_timestamp_meta
                    //     .get_timestamp()
                    //     .nanoseconds()
                    //     .unwrap()
                );

                Ok(gst::FlowSuccess::Ok)
            })
            .build(),
    );

    appsrc.get_static_pad("src").unwrap().add_probe(
        gst::PadProbeType::BUFFER,
        move |_pad, info| {
            if let Some(gst::PadProbeData::Buffer(ref mut buffer)) = &mut info.data {
                let buffer = buffer.make_mut();
                let label = "Test label from example";
                custom_meta::CustomMeta::add(buffer, label.to_string());
            }
            gst::PadProbeReturn::Ok
        },
    );

    // Actually start the pipeline.
    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");
    let pipeline = pipeline.dynamic_cast::<gst::Pipeline>().unwrap();

    let bus = pipeline
        .get_bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    // And run until EOS or an error happened.
    for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => break,
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                break;
            }
            _ => (),
        }
    }

    // Finally shut down everything.
    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");
}

fn main() {
    example_main();
}
